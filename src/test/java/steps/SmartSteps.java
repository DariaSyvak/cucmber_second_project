package steps;

import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pages.SmartHomePage;
import pages.SmartOrdersPage;
import utils.Driver;
import utils.DropDownHandler;
import utils.Waiter;

    public class SmartSteps {

        WebDriver driver;
        SmartHomePage smartHomePage;
        SmartOrdersPage smartOrdersPage;

        @Before
        public void setup() {
            driver = Driver.getDriver();
            smartHomePage = new SmartHomePage();
            smartOrdersPage = new SmartOrdersPage();
        }

        @When("user enters username as{string}")
        public void user_enters_username_as(String userName) {
            smartHomePage.userNameInputBox.sendKeys(userName);

        }

        @When("user enters password as{string}")
        public void user_enters_password_as(String password) {
            smartHomePage.passwordInputBox.sendKeys(password);
        }

        @And("user clicks on Login button")
        public void user_clicks_on_Login_button() {
            smartHomePage.loginButton.click();

        }
        @Then("user should see{string} Message")
        public void user_should_see_Message(String invalidMessage) {
            Assert.assertEquals(invalidMessage, smartHomePage.errorMessage.getText());

        }

        @Then("user should be routed to{string}")
        public void userShouldBeRoutedTo(String currentUrl) {
            Assert.assertEquals(currentUrl, driver.getCurrentUrl());
        }

        @And("validate below menu items are displayed")
        public void validateBelowMenuItemsAreDisplayed(DataTable dataTable) {
            for (int i = 0; i < dataTable.asList().size(); i++) {
                Assert.assertTrue(smartOrdersPage.ordersMenuItems.get(i).isDisplayed());
            }
        }

        @When("user clicks on{string} button")
        public void user_clicks_on_button(String button) {
            switch (button) {
                case "Check All":
                    smartOrdersPage.checkAllBox.click();
                    break;

                case "Uncheck All":
                    smartOrdersPage.unCheckAllBox.click();
                    break;

                case "Delete Selected":
                    smartOrdersPage.deleteButton.click();
                    break;
            }
        }

        @Then("all rows should be checked")
        public void all_rows_should_be_checked() {
            for (WebElement element : smartOrdersPage.checkBoxes) {
                Assert.assertFalse(element.isSelected());
            }
        }
        @Then("all rows should be unchecked")
        public void all_rows_should_be_unchecked() {
            for (WebElement element : smartOrdersPage.checkBoxes) {
                Assert.assertFalse(element.isSelected());
            }
        }

        @When("user clicks on{string} menu item")
        public void user_clicks_on_menu_item(String orderMenuItem) {
            switch (orderMenuItem) {
                case "Order":
                    for (int i = 0; i < smartOrdersPage.ordersMenuItems.size(); i++) {
                        smartOrdersPage.ordersMenuItems.get(i).click();
                    }
            }
        }

        @When("user selects{string} as product")
        public void user_selects_as_product(String product) {
            Waiter.pause(3);
            DropDownHandler.selectOptionByValue(smartOrdersPage.familyAlbumProduct, product);

        }

        @When("user enters {int} as quantity")
        public void user_enters_as_quantity(Integer quantity) {
            smartOrdersPage.quantityInputBox.sendKeys(String.valueOf(quantity));
        }

        @When("user enters all address information")
        public void user_enters_all_address_information() {
            smartOrdersPage.customerNameInputBox.sendKeys("Daria");
            smartOrdersPage.StreetInputBox.sendKeys("Castilian ct");
            smartOrdersPage.cityInputBox.sendKeys("Glenview");
            smartOrdersPage.StateInputBox.sendKeys("IL");
            smartOrdersPage.zipInputBox.sendKeys("60025");

        }

        @When("user enters all payment information")
        public void user_enters_all_payment_information() {
            smartOrdersPage.visaRadioButton.click();
            smartOrdersPage.cardNrInputBox.sendKeys("1234567890");
            smartOrdersPage.expireDateInputBox.sendKeys("13/23");
            smartOrdersPage.processBox.click();

        }

        @And("user clicks on{string} menu items")
        public void userClicksOnMenuItems(String viewAllOrderLink) {
            smartOrdersPage.viewAllOrder.click();
        }

        @Then("user should see their order displayed in the{string} table")
        public void user_should_see_their_order_displayed_in_the_table(String string) {
            for (int i = 1; i < smartOrdersPage.myOrderFirstRow.size()-1; i++) {
                Assert.assertTrue(smartOrdersPage.myOrderFirstRow.get(i).isDisplayed());
            }

        }
        @Then("validate all information entered displayed correct with the order")
        public void validate_all_information_entered_displayed_correct_with_the_order(DataTable dataTable) {
            for (int i = 1; i <12; i++) {
                Assert.assertEquals(dataTable.asList().get(i), smartOrdersPage.myOrderFirstRow.get(i).getText());
            }
        }

        @Then("validate all orders are deleted from the{string}")
        public void validate_all_orders_are_deleted_from_the(String orders) {
            Assert.assertNotNull(orders);

        }
        @Then("validate user sees{string}Message")
        public void validate_user_sees_Message(String emptyMessage) {
            Assert.assertEquals(emptyMessage,smartOrdersPage.emptyMessage.getText());
        }
    }

