This Is BDD-UI framework.
It has dependencies Selenium-java ,Web driver manager Boni Garcia ,Cucumber java and jUnit,HtmlUnitDriver.
It has  configuration.properties file, and ConfigReader, Driver, Waiter utility
classes,and design pattern to increase readability framework, Singleton to provide a single instance of driver to be used for scripts
Page Object Model to centralize, maintain and reuse Web Elements and related page methods in object repositories.
Project has separate pages for each html page
Feature file as “smart.feature” I use (Given , When ,Then )annotations and write my step definitions.
You can run test in “Runner” class with tags"Smoke"or "Regression "
 or  mvn command : mva test -Dcucumber.options=”—tags @Smoke” 
